package com.siloenix;

import java.io.*;
import java.util.ArrayList;
import java.util.zip.Adler32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Hello world!
 *
 */
public class App implements Serializable
{
    int[] a = new int[3000];
    public static void main( String[] args ) throws IOException, ClassNotFoundException {
        long start = System.nanoTime();
        String path = "/home/yaroslav/IdeaProjects/";
        File f = new File(path + "data");

        ObjectOutputStream o = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(f)));
        Ext ext = new Ext();
//        ext.a = 10;

        o.writeObject(ext);
        o.close();

        ObjectInputStream i = new ObjectInputStream(new FileInputStream(f));
        Object ob = i.readObject();
        i.close();

//        CheckedOutputStream csum = new CheckedOutputStream(
//                new FileOutputStream(new File(path + "testable.zip")), new Adler32()
//        );
//        ZipOutputStream zos = new ZipOutputStream(csum);
//        BufferedOutputStream out = new BufferedOutputStream(zos);
//        zos.putNextEntry(new ZipEntry("bad" + File.separator + f.getName()));
//
//        BufferedReader r = new BufferedReader(new FileReader(f));
//
//        int b;
//        while ((b = r.read()) != -1)
//            out.write(b);
//
//        out.close();
//        r.close();
        System.out.println(System.nanoTime() - start);
        run(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    private static void run(Runnable runnable){
        runnable.run();
    }
}

class Ext implements Externalizable{
    int[] a = new int[100];

    public Ext() {
        System.out.println("ext ctor");
    }

    public void writeExternal(ObjectOutput out) throws IOException {
        System.out.println("ext written, a = " + a);
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        System.out.println("ext read, a = " + a);
    }
}
