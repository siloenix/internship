package com.siloenix.utils;

import java.io.Serializable;


/**
 * allows to deserialize (restore) parts of an object, not serialized fully
 * */
public interface SerializationDependency<T> {
    Serializable getSerializable();
    void restoreSerializable(T object);
}
