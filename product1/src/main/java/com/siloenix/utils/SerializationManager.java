package com.siloenix.utils;

import java.io.*;
import java.util.*;

/**
 * serializes and deserializes all serialization dependencies
 * @see SerializationDependency
 * */
public class SerializationManager {
    private String serializationPath; /*= */

    private LinkedList<SerializationDependency> serializationList = new LinkedList<>();

    public SerializationManager(String serializationPath, SerializationDependency... dependencies) {
        this.serializationPath = serializationPath;
        serializationList.addAll(Arrays.asList(dependencies));
    }

    public void serialize() throws IOException {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(serializationPath))) {
            for (SerializationDependency serializationDependency : serializationList) {
                out.writeObject(serializationDependency.getSerializable());
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void deserialize() throws IOException {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(serializationPath))) {
            for (SerializationDependency serializationDependency : serializationList) {
                serializationDependency.restoreSerializable(in.readObject());
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
