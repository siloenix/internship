package com.siloenix.utils;

public class Preferences {
    public static String PATH_TO_NON_SPOILING_PRODUCTS = Preferences.class
                .getResource("/product_names/non_spoiling_products")
                .getPath();
    public static String PATH_TO_SPOILING_PRODUCTS = Preferences.class
                .getResource("/product_names/spoiling_products")
                .getPath();
    public static String PATH_TO_FAST_SPOILING_PRODUCTS = Preferences.class
                .getResource("/product_names/fast_spoiling_products")
                .getPath();
    public static String PATH_TO_CONTAINER_DATA = Preferences.class
                .getResource("/appdata/container.data")
                .getPath();
}
