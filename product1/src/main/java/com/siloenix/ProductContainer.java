package com.siloenix;

import com.siloenix.products.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class ProductContainer extends ArrayList<Product> {
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Product Container (" + size() + "):\n[\n");
        for (Product product : this) {
            sb.append("\t").append(product).append("\n");
        }
        sb.append("]\n");
        return sb.toString();
    }
}
