package com.siloenix;

import com.siloenix.products.Comparators;
import com.siloenix.products.Product;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.HashMap;

public class ConsoleControls {
    private HashMap<String, Runnable> controlsMap = new HashMap<>();
    private App app;

    private Runnable serialize = () -> {
        try {
            app.serializationManager.serialize();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.err.println("container serialized");
    };

    private Runnable deserialize = () -> {
        try {
            app.serializationManager.deserialize();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.err.println("container deserialized");
    };

    private Runnable printContainer = () -> {
        System.out.println(app.container);
    };

    private Runnable sortAscending = () -> {
        app.container.sort(Comparators.all().expirationDateComparator(true));
        System.err.println("container sorted ascending");
    };

    private Runnable sortDescending = () -> {
        app.container.sort(Comparators.all().expirationDateComparator(false));
        System.err.println("container sorted descending");
    };

    private Runnable sortNAscending = () -> {
        app.container.sort(Comparators.all().nameComparator(true));
        System.err.println("container sorted by name ascending");
    };

    private Runnable sortNDescending = () -> {
        app.container.sort(Comparators.all().nameComparator(false));
        System.err.println("container sorted by name descending");
    };

    private Runnable addRandom = () -> {
        Product product = app.generator.next();
        app.container.add(product);
        System.err.println("added to container : '" + product + "'");
    };

//    private Runnable addFromCustom = () -> {
//        System.err.println("added to container : '" + app.container.addRandomFromCustomSource() + "'");
//    };
//
//    private Runnable setNamesSource = () -> {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        System.out.println("enter the source file name:");
//        String source;
//        try {
//            source = reader.readLine();
//            File file = new File(source);
//            if (!file.exists() || !file.canRead() || !file.isFile())
//                throw new IOException("not compatible file");
//            else {
//                app.container.setCustomFactorySource(file);
//                System.err.println("source file for custom Comparators.all()s set to: '" + source + "'");
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    };

    private Runnable exitWithSerialize = () -> {
        serialize.run();
        System.exit(0);
    };

    private Runnable exitNotSerialized = () -> {
        System.err.println("container not serialized");
        System.exit(0);
    };

    private Runnable returnToApp = () -> {
        throw new RuntimeException("execution given to App");
    };

    ConsoleControls(App app){
        this.app = app;
        Class<ConsoleControls> c = ConsoleControls.class;
        try {
            for (Field field : c.getDeclaredFields()) {
                field.setAccessible(true);
                String name = field.getName();
                Object f = field.get(this);
                if (f instanceof Runnable) {
                    controlsMap.put(name, (Runnable) f);
                }
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public void printControls(){
        StringBuilder sb = new StringBuilder("Available commands:\n");
        for (String s : controlsMap.keySet()) {
            sb.append("\t'").append(s).append("'\n");
        }
        System.out.println(sb);
    }

    public void execute() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String command;

        while (true){
            printControls();
            try {
                command = reader.readLine();
                System.out.println();
                Runnable control = controlsMap.get(command);

                if (control != null)
                    control.run();
                else if (!command.equals("")){
                    for (String s : controlsMap.keySet()) {
                        if (s.startsWith(command))
                            controlsMap.get(s).run();
                    }
                }
            } catch (RuntimeException e) {
                System.out.println(e.getMessage());
                break;
            }
        }
    }
}
