package com.siloenix.products;

import java.util.Comparator;

public class Comparators {

    private static Comparators comparators = new Comparators();
    public static Comparators all() {
        return comparators;
    }

    private Comparators() {}

    public Comparator<Product> expirationDateComparator(final boolean ascending){
        return new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                if (ascending)
                    return o1.compareTo(o2);
                else
                    return o2.compareTo(o1);
            }
        };
    }
    public Comparator<Product> nameComparator(final boolean ascending){
        return new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                if (ascending)
                    return o1.name.toUpperCase().compareTo(o2.name.toUpperCase());
                else
                    return o2.name.toUpperCase().compareTo(o1.name.toUpperCase());
            }
        };
    }

}
