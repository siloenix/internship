package com.siloenix.products;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class FastSpoilingProduct extends SpoilingProduct{
//    private static File nameSources;
//    static {
//        String pathToNames = NonSpoilingProduct.class
//                .getResource("/product_names/fast_spoiling_products")
//                .getPath();
//
//        nameSources = new File(pathToNames);
//    }
//    public static FileProductFactory factory = new FileProductFactory(nameSources) {
//        @Override
//        public Product createRandom() throws IOException {
//            ArrayList<String> names = getNamesFromSource();
//            return new FastSpoilingProduct(
//                    names.get(new Random(new Date().getTime()).nextInt(names.size()))
//            );
//        }
//    };

    FastSpoilingProduct(String name) {
        super(name);
    }

    @Override
    Date getRandomDateExpiration() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.SECOND, random.nextInt(40) + 10);
        return c.getTime();
    }
}
