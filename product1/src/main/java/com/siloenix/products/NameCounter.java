package com.siloenix.products;

import com.siloenix.utils.SerializationDependency;

import java.io.Serializable;
import java.util.HashMap;

public class NameCounter implements Serializable {
    private static NameCounter counter = new NameCounter();

    private HashMap<String, Integer> nameCounter = new HashMap<>();
    private int count(String name) {
        Integer count = nameCounter.get(name);
        count = (count == null) ? 0 : count + 1;
        nameCounter.put(name, count);
        return count;
    }

    public static int getCount(String name) {
        return counter.count(name);
    }

    public static SerializationDependency getSerializationDependency(){
        return new SerializationDependency<NameCounter>() {
            @Override
            public Serializable getSerializable() {
                return counter;
            }

            @Override
            public void restoreSerializable(NameCounter object) {
                counter = object;
            }
        };
    }
}
