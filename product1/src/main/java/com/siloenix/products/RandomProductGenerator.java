package com.siloenix.products;

import java.util.*;

public class RandomProductGenerator {
    private Random random = new Random(new Date().getTime());
    private ArrayList<ProductFactory> factories = new ArrayList<>();

    public void addFactory(ProductFactory factory) {
        factories.add(factory);
    }

    public Product next() {
        return factories.get(random.nextInt(factories.size())).create();
    }

    public void fill(Collection<Product> products, int amount) {
        for (int i = 0; i < amount; i++) {
            products.add(next());
        }
    }
}
