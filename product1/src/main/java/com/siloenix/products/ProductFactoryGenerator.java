package com.siloenix.products;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class ProductFactoryGenerator {
    public static FileProductFactory createNonSpoilingProductFactory(@NotNull File nameSource) throws IOException {
        checkFile(nameSource);
        return new FileProductFactory(nameSource) {
            @Override
            public Product createRandom() throws IOException {
                ArrayList<String> names = getNamesFromSource();
                return new NonSpoilingProduct(
                        names.get(new Random(new Date().getTime()).nextInt(names.size()))
                );
            }
        };
    }

    public static FileProductFactory createSpoilingProductFactory(@NotNull File nameSource) throws IOException {
        checkFile(nameSource);
        return new FileProductFactory(nameSource) {
            @Override
            public Product createRandom() throws IOException {
                ArrayList<String> names = getNamesFromSource();
                return new SpoilingProduct(
                        names.get(new Random(new Date().getTime()).nextInt(names.size()))
                );
            }
        };
    }

    public static FileProductFactory createFastSpoilingProductFactory(@NotNull File nameSource) throws IOException {
        checkFile(nameSource);
        return new FileProductFactory(nameSource) {
            @Override
            public Product createRandom() throws IOException {
                ArrayList<String> names = getNamesFromSource();
                return new FastSpoilingProduct(
                        names.get(new Random(new Date().getTime()).nextInt(names.size()))
                );
            }
        };
    }

    private static void checkFile(File file) throws IOException{
        if (!file.exists() || !file.isFile() || !file.canRead())
            throw new IOException("bad file");
    }
}
