package com.siloenix.products;

import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class NonSpoilingProduct extends Product {
//    private static File nameSources;
//    static {
//        String pathToNames = NonSpoilingProduct.class
//                .getResource("/product_names/non_spoiling_products")
//                .getPath();
//
//        nameSources = new File(pathToNames);
//    }
//    public static FileProductFactory factory = new FileProductFactory(nameSources) {
//        @Override
//        public Product createRandom() throws IOException {
//            ArrayList<String> names = getNamesFromSource();
//            return new NonSpoilingProduct(
//                    names.get(new Random(new Date().getTime()).nextInt(names.size()))
//            );
//        }
//    };

    NonSpoilingProduct(String name){
        super(name);
    }

    @Override
    public int compareTo(@NotNull Product o) {
        return (o instanceof NonSpoilingProduct) ? 0 : 1;
    }

    @Override
    public String toString() {
        return String.format("%-25s", super.toString()) + "\tDate Expiration: None";
    }
}
