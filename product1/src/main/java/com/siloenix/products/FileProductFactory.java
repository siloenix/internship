package com.siloenix.products;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public abstract class FileProductFactory implements ProductFactory{
    private File nameSources;

    FileProductFactory(File nameSource) {
        this.nameSources = nameSource;
    }

    abstract Product createRandom() throws IOException;
    public void setNamesSource(File file){
        nameSources = file;
    }

    ArrayList<String> getNamesFromSource() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(nameSources));
        ArrayList<String> names = new ArrayList<>();
        String s;
        while ((s = reader.readLine()) != null)
            names.add(s);
        return names;
    }

    @Override
    public FileProductFactory clone(){
        try {
             return  (FileProductFactory) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Product create() {
        try {
            return createRandom();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
