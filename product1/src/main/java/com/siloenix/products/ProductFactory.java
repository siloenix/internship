package com.siloenix.products;

interface ProductFactory extends Cloneable{
    Product create();
}
