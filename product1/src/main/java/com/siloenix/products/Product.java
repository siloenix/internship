package com.siloenix.products;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

public abstract class Product implements Comparable<Product>, Serializable{
    String name;
    private int count;
    static Random random = new Random(new Date().getTime());

    Product(String name) {
        this.name = name;
        count = NameCounter.getCount(name);
    }

    @Override
    public String toString() {
        return name + " " + count;
    }


}
