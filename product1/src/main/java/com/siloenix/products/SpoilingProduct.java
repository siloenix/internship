package com.siloenix.products;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class SpoilingProduct extends Product{
    private Date dateExpiration;
//    static File nameSources;
//    static {
//        String pathToNames = NonSpoilingProduct.class
//                .getResource("/product_names/spoiling_products")
//                .getFile();
//
//        nameSources = new File(pathToNames);
//    }
//    public static FileProductFactory factory = new FileProductFactory(nameSources) {
//        @Override
//        public Product createRandom() throws IOException {
//            ArrayList<String> names = getNamesFromSource();
//            return new SpoilingProduct(
//                    names.get(new Random(new Date().getTime()).nextInt(names.size()))
//            );
//        }
//    };


    SpoilingProduct(String name){
        super(name);
        dateExpiration = getRandomDateExpiration();
    }

    Date getRandomDateExpiration(){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, random.nextInt(2) + 1);
        return c.getTime();
    }

    @Override
    public int compareTo(@NotNull Product o) {
        if (o instanceof SpoilingProduct)
            return dateExpiration.compareTo(((SpoilingProduct)o).dateExpiration);
        else
            return -1;
    }

    @Override
    public String toString() {
        return String.format("%-25s", super.toString()) + "\tDate Expiration: "
                + new SimpleDateFormat("HH:mm:ss").format(dateExpiration);
    }
}
