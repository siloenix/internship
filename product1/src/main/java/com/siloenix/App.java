package com.siloenix;

import com.siloenix.products.NameCounter;
import com.siloenix.products.ProductFactoryGenerator;
import com.siloenix.products.RandomProductGenerator;
import com.siloenix.utils.Preferences;
import com.siloenix.utils.SerializationDependency;
import com.siloenix.utils.SerializationManager;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

/**
 * Hello world!
 *
 */
public class App implements SerializationDependency
{
    private String pathToContainerData = Preferences.PATH_TO_CONTAINER_DATA;

    ProductContainer container;
    RandomProductGenerator generator = new RandomProductGenerator();

    SerializationManager serializationManager
            = new SerializationManager(pathToContainerData, this, NameCounter.getSerializationDependency());

    private App() throws IOException {
        generator.addFactory(ProductFactoryGenerator.createSpoilingProductFactory(
                new File(Preferences.PATH_TO_SPOILING_PRODUCTS)));
        generator.addFactory(ProductFactoryGenerator.createFastSpoilingProductFactory(
                new File(Preferences.PATH_TO_FAST_SPOILING_PRODUCTS)));
        generator.addFactory(ProductFactoryGenerator.createNonSpoilingProductFactory(
                new File(Preferences.PATH_TO_NON_SPOILING_PRODUCTS)));
        try {
            serializationManager.deserialize();
        } catch (EOFException e) {
            System.err.println("no data at 'container.data' file");
            container = new ProductContainer();
            generator.fill(container, 30);
        } catch (IOException e) {
            System.err.println("file corrupted");
            container = new ProductContainer();
            generator.fill(container, 30);
        }
    }

    public static void main( String[] args ) throws IOException {
        App app = new App();
        ConsoleControls controls = new ConsoleControls(app);

        controls.execute();

        System.err.println("container not saved");
    }

    @Override
    public Serializable getSerializable() {
        return container;
    }

    @Override
    public void restoreSerializable(Object object) {
        container = (ProductContainer) object;
    }


}
